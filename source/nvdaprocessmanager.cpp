#include "nvdaprocessmanager.h"

#include <QProcess>

#include <windows.h>
#include <tlhelp32.h>
#include <tchar.h>

NVDAProcessManager::NVDAProcessManager(QObject *parent) : QObject(parent)
{

}

bool NVDAProcessManager::isNVDARunning()
{
  const TCHAR* exeName { L"nvda.exe" };

  PROCESSENTRY32 entry;
  entry.dwSize = sizeof (PROCESSENTRY32);

  struct Snapshot {
    Snapshot() : h { CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0) } {}
    ~Snapshot() { CloseHandle(h); }
    HANDLE h;
  } snapshot;

  if (!Process32First(snapshot.h, &entry)) return false;

  do if (!_tcsicmp(entry.szExeFile, exeName)) return true;
  while (Process32Next(snapshot.h, &entry));

  return false;
}

void NVDAProcessManager::setNVDA_Path(const QString &path)
{
  m_NVDAPath = path;
}

bool NVDAProcessManager::runNVDA()
{
  QProcess::execute(m_NVDAPath);
  emit NVDA_Started();
  return true;
}
