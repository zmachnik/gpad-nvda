#ifndef BRLETTER_H
#define BRLETTER_H

#include <array>
#include <map>
#include <QString>

class BrLetter
{
public:
  BrLetter();

  bool & operator[](int index);
  operator wchar_t() const;
  QString dots() const;

private:
  static std::map<std::array<bool, 8>, wchar_t> m_dict;

  std::array<bool, 8> m_dots = {0, 0, 0, 0, 0, 0, 0, 0};
};

#endif // BRLETTER_H
