#ifndef COMMANDTRIGGER_H
#define COMMANDTRIGGER_H

#include <QObject>

#include "gamepadlistener.h"
#include "brletter.h"

class CommandTrigger : public QObject
{
  Q_OBJECT
public:
  explicit CommandTrigger(QObject *parent = nullptr);

  enum class Mode {
    DesktopApp1, WebBrowsing1, TextInput1, Settings1,
    DesktopApp2, WebBrowsing2, TextInput2, Settings2
  };

public slots:
  void translateButton(GamepadListener::DigitalButton button);

private:

  Mode m_mode = Mode::DesktopApp1;

  void setMode(Mode mode);

  void WIN_Up();
  void WIN_Down();
  void WIN_Left();
  void WIN_Right();

  void WIN_VolumeUp();
  void WIN_VolumeDown();

  void WIN_PrevElement();
  void WIN_NextElement();
  void WIN_PrevWindow();
  void WIN_NextWindow();
  void WIN_NewWindow();
  void WIN_CloseWindow();
  void WIN_PrevTaskBarItem();
  void WIN_NextTaskBarItem();

  void WIN_Activate();
  void WIN_Cancel();
  void WIN_Space();
  void WIN_Backspace();
  void WIN_Delete();
  void WIN_Rename();
  void WIN_NewDirectory();
  void WIN_Home();
  void WIN_End();

  void WIN_ContextMenu();
  void WIN_StartMenu();
  void WIN_ApplicationMenu();

  void WIN_PrevTab();
  void WIN_NextTab();
  void WIN_NewTab();
  void WIN_CloseTab();
  void WIN_PrevSite();
  void WIN_NextSite();
  void WIN_RefreshSite();
  void WIN_StopSite();
  void WIN_FocusAddressBar();

  void WIN_Undo();
  void WIN_Redo();

  void WIN_SelectAll();
  void WIN_Copy();
  void WIN_Cut();
  void WIN_Paste();

  void WIN_Search();
  void WIN_OpenWinSettings();
  void WIN_OpenActionCenter();
  void WIN_SystemProperties();

  void WIN_ExpandFolder();
  void WIN_ChangeViewToList();
  void WIN_ItemProperties();
  void WIN_FolderUp();

  void NVDA_StopReading();
  void NVDA_PauseResumeReading();
  void NVDA_ReadFromCurrentPosition();

  void NVDA_PrevHeading();
  void NVDA_NextHeading();
  void NVDA_ListAllHeadings();

  void NVDA_PrevForm();
  void NVDA_NextForm();

  void NVDA_PrevParagraph();
  void NVDA_NextParagraph();

  void NVDA_PrevTable();
  void NVDA_NextTable();
  void NVDA_PrevCellInRow();
  void NVDA_NextCellInRow();
  void NVDA_PrevCellInColumn();
  void NVDA_NextCellInColumn();

  void NVDA_PrevLink();
  void NVDA_NextLink();
  void NVDA_ListAllLinks();

  void NVDA_SayClipboard();
  void NVDA_SayCurrentObject();
  void NVDA_SayTitleBar();
  void NVDA_SayWindow();
  void NVDA_SayStatusBar();

  void NVDA_ToggleBrowseFocusMode();
  void NVDA_ElementsListDialog();
  void WIN_FindDialog();

  void NVDA_NVDAMenu();
  void NVDA_GeneralSettings();
  void NVDA_SpeakSlower();
  void NVDA_SpeakFaster();
  void NVDA_ChangeLanguage();
  void NVDA_ToggleSpeakTypedWords();
  void NVDA_ToggleSpeakCommandKeys();
  void NVDA_ToggleSpeakPunktuation();

  void NVDA_SayTime();
  void NVDA_SayBattery();

  void WIN_InitDictation();

  void pressKey(int keyCode, bool insert = false, bool ctrl = false, bool alt = false,
                bool shift = false, bool extended = false, bool super = false);

  static void toggle(bool & b);

  bool m_eightPoint = false;
  int m_currBrRow = 1;
  BrLetter m_letter;
  bool leftDotDone = true;
  bool rightDotDone = true;
  void brLetterEntry();


signals:

  void logEvent(QString message);
  void modeChanged(Mode mode);
  void eightPointChanged(bool on);
};

#endif // COMMANDTRIGGER_H
