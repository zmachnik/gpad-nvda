#ifndef GAMEPADLISTENER_H
#define GAMEPADLISTENER_H

#include <QObject>
#include <QtGamepad/QGamepad>
#include <memory>


class GamepadListener : public QObject
{
  Q_OBJECT

public:

  enum class DigitalButton {
    A_Pressed, A_Released, B_Pressed, B_Released, Y_Pressed, Y_Released, X_Pressed, X_Released, Up_Pressed, Down_Pressed, Left_Pressed, Right_Pressed,
    Up_Released, Down_Released, Left_Released, Right_Released, L1_Pressed, L1_Released, L2_Pressed, L2_Released, L3_Pressed,
    R1_Pressed, R1_Released, R2_Pressed, R2_Released, R3_Pressed, LS_Up_Pressed, LS_Down_Pressed, LS_Left_Pressed, LS_Right_Pressed,
    RS_Up_Pressed, RS_Down_Pressed, RS_Left_Pressed, RS_Right_Pressed, Select_Pressed, Start_Pressed, Guide_Pressed,
    Guide_Released
  };

  GamepadListener();

  void initDefaultGamepad();

private:

  std::unique_ptr<QGamepad> m_gamepad;

  enum class StickPosition {
    Center, Up, Down, Left, Right
  }
  m_currLStickDirection = StickPosition::Center,
  m_currRStickDirection = StickPosition::Center;

  StickPosition stickToButton(double x, double y);

  bool m_currLTriggerPosition = false;
  bool m_currRTriggerPosition = false;

  bool triggerToButton(double x);


private slots:

  void digitizeLStick();
  void digitizeRStick();

  void digitizeLTrigger();
  void digitizeRTrigger();

signals:

  void buttonAction(DigitalButton);

  void noGamepadFound();

  void gamepadConnected();
  void gamepadDisconnected();
};

#endif // GAMEPADLISTENER_H
