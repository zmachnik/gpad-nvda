#ifndef AUDIONOTIFY_H
#define AUDIONOTIFY_H

#include <QObject>
#include <QSoundEffect>
#include "tts.h"

class AudioNotify : public QObject
{
  Q_OBJECT

public:
  AudioNotify(const TTS & tts);

public slots:

  void startUp();

  void desktopApp(bool pressed);
  void webBrowsing(bool pressed);
  void textInput(bool pressed);
  void settings(bool pressed);

  void eightPoint(bool on);

protected:
  const TTS & m_tts;

private:
  QSoundEffect m_startUp;
};

#endif // AUDIONOTIFY_H
