#include "commandtrigger.h"

#include <windows.h>

CommandTrigger::CommandTrigger(QObject *parent) : QObject(parent)
{

}

void CommandTrigger::translateButton(GamepadListener::DigitalButton button)
{
  switch (button) {

      // Modes

    case GamepadListener::DigitalButton::Up_Pressed: return setMode(Mode::WebBrowsing2);
    case GamepadListener::DigitalButton::Down_Pressed: return setMode(Mode::TextInput2);
    case GamepadListener::DigitalButton::Left_Pressed: return setMode(Mode::Settings2);
    case GamepadListener::DigitalButton::Right_Pressed: return setMode(Mode::DesktopApp2);

    case GamepadListener::DigitalButton::Up_Released: return setMode(Mode::WebBrowsing1);
    case GamepadListener::DigitalButton::Down_Released: return setMode(Mode::TextInput1);
    case GamepadListener::DigitalButton::Left_Released: return setMode(Mode::Settings1);
    case GamepadListener::DigitalButton::Right_Released: return setMode(Mode::DesktopApp1);

    case GamepadListener::DigitalButton::LS_Up_Pressed: {
        if (m_mode == Mode::DesktopApp2) return WIN_FolderUp();
        if (m_mode == Mode::WebBrowsing1) return NVDA_PrevHeading();
        if (m_mode == Mode::WebBrowsing2) return WIN_FindDialog();
        return WIN_Up();
      }
    case GamepadListener::DigitalButton::LS_Down_Pressed: {
        if (m_mode == Mode::DesktopApp2) return WIN_ExpandFolder();
        if (m_mode == Mode::WebBrowsing1) return NVDA_NextHeading();
        if (m_mode == Mode::WebBrowsing2) return WIN_Search();
        return WIN_Down();
      }
    case GamepadListener::DigitalButton::LS_Left_Pressed:  {
        if (m_mode == Mode::DesktopApp2) return WIN_PrevTaskBarItem();
        if (m_mode == Mode::WebBrowsing1) return NVDA_PrevForm();
        if (m_mode == Mode::WebBrowsing2) return WIN_PrevSite();
        return WIN_Left();
      }
    case GamepadListener::DigitalButton::LS_Right_Pressed: {
        if (m_mode == Mode::DesktopApp2) return WIN_NextTaskBarItem();
        if (m_mode == Mode::WebBrowsing1) return NVDA_NextForm();
        if (m_mode == Mode::WebBrowsing2) return WIN_NextSite();
        return WIN_Right();
      }

      // Actions

    case GamepadListener::DigitalButton::RS_Up_Pressed: {
        if (m_mode == Mode::DesktopApp2) return NVDA_SayTitleBar();
        if (m_mode == Mode::Settings1 || m_mode == Mode::Settings2) return WIN_VolumeUp();
        return NVDA_StopReading();
      }
    case GamepadListener::DigitalButton::RS_Down_Pressed: {
        if (m_mode == Mode::DesktopApp2) return NVDA_SayStatusBar();
        if (m_mode == Mode::Settings1 || m_mode == Mode::Settings2) return WIN_VolumeDown();
        return NVDA_PauseResumeReading();
      }
    case GamepadListener::DigitalButton::RS_Left_Pressed: {
        if (m_mode == Mode::DesktopApp2) return NVDA_SayWindow();
        if (m_mode == Mode::Settings1 || m_mode == Mode::Settings2) return NVDA_SpeakSlower();
        return NVDA_SayCurrentObject();
      }
    case GamepadListener::DigitalButton::RS_Right_Pressed: {
        //if (m_mode == Mode::DesktopApp2)
        if (m_mode == Mode::Settings1 || m_mode == Mode::Settings2) return NVDA_SpeakFaster();
        return NVDA_ReadFromCurrentPosition();
      }

    case GamepadListener::DigitalButton::L1_Pressed: {
        if (m_mode == Mode::DesktopApp2) return WIN_CloseWindow();
        if (m_mode == Mode::WebBrowsing1) return WIN_PrevTab();
        if (m_mode == Mode::WebBrowsing2) return WIN_CloseTab();
        if (m_mode == Mode::TextInput1 || m_mode == Mode::TextInput2) return WIN_Home();
        return WIN_PrevWindow();
      }

    case GamepadListener::DigitalButton::R1_Pressed: {
        if (m_mode == Mode::DesktopApp2) return WIN_NewWindow();
        if (m_mode == Mode::WebBrowsing1) return WIN_NextTab();
        if (m_mode == Mode::WebBrowsing2) return WIN_NewTab();
        if (m_mode == Mode::TextInput1 || m_mode == Mode::TextInput2) return WIN_End();
        return WIN_NextWindow();
      }

    case GamepadListener::DigitalButton::L2_Pressed: {
        logEvent("L2_Pressed");
        if (m_mode == Mode::WebBrowsing2) return NVDA_PrevLink();
        if (m_mode == Mode::TextInput2) {
            int index = (m_currBrRow-1)*2;
            toggle(m_letter[index]);
            logEvent(QString{"Letter changed to "} + m_letter.dots());
            leftDotDone = false;
            break;
          }
        else if (m_mode == Mode::DesktopApp2) return WIN_Undo();
        return WIN_PrevElement();
      }
    case GamepadListener::DigitalButton::R2_Pressed: {
        logEvent("R2_Pressed");
        if (m_mode == Mode::WebBrowsing2) return NVDA_NextLink();
        if (m_mode == Mode::TextInput2) {
            int index = (m_currBrRow-1)*2+1;
            toggle(m_letter[index]);
            logEvent(QString{"Letter changed to "} + m_letter.dots());
            rightDotDone = false;
            break;
          }
        else if (m_mode == Mode::DesktopApp2) return WIN_Redo();
        return WIN_NextElement();
      }

    case GamepadListener::DigitalButton::L2_Released: {
        logEvent("L2_Released");
        if (m_mode == Mode::TextInput2) {
            leftDotDone = true;
            //logEvent("a1");
            if (rightDotDone) {
                //logEvent("a2");
                if (m_eightPoint && m_currBrRow < 4) ++m_currBrRow;
                else if (m_currBrRow < 3) ++m_currBrRow;
                else brLetterEntry();
              }
          }
        break;
      }
    case GamepadListener::DigitalButton::R2_Released: {
        logEvent("R2_Released");
        if (m_mode == Mode::TextInput2) {
            rightDotDone = true;
            if (leftDotDone) {
                if (m_eightPoint && m_currBrRow < 4) ++m_currBrRow;
                else if (m_currBrRow < 3) ++m_currBrRow;
                else brLetterEntry();
              }
          }
        break;
      }

    case GamepadListener::DigitalButton::L3_Pressed: {
        if (m_mode == Mode::WebBrowsing1 || m_mode == Mode::WebBrowsing2
            || m_mode == Mode::TextInput1 || m_mode == Mode::TextInput2) return NVDA_ToggleBrowseFocusMode();
        if (m_mode == Mode::DesktopApp2) return WIN_NewDirectory();
        return WIN_ApplicationMenu();
      }
    case GamepadListener::DigitalButton::R3_Pressed: {
        if (m_mode == Mode::DesktopApp2) return NVDA_SayClipboard();
        if (m_mode == Mode::Settings1 || m_mode == Mode::Settings2) return WIN_ItemProperties();
        return WIN_InitDictation();
      }

    case GamepadListener::DigitalButton::A_Pressed: {
        if (m_mode == Mode::DesktopApp2) return WIN_Paste();
        return WIN_Activate();
      }
    case GamepadListener::DigitalButton::B_Pressed: {
        if (m_mode == Mode::TextInput2)
          return WIN_Backspace();
        if (m_mode == Mode::DesktopApp2) return WIN_Cut();
        if (m_mode == Mode::Settings1 || m_mode == Mode::Settings2) return NVDA_ToggleSpeakPunktuation();
        if (m_mode == Mode::WebBrowsing2) return WIN_StopSite();
        return WIN_Cancel();
      }
    case GamepadListener::DigitalButton::X_Pressed: {
        if (m_mode == Mode::TextInput2) {
            if (leftDotDone && rightDotDone) {
                logEvent(QString{"Letter changed to "} + m_letter.dots());
                if (m_eightPoint && m_currBrRow < 4) ++m_currBrRow;
                else if (m_currBrRow < 3) ++m_currBrRow;
                else brLetterEntry();
              }
            break;
          }
        else if (m_mode == Mode::DesktopApp2) return WIN_Copy();
        else if (m_mode == Mode::Settings1) return NVDA_ToggleSpeakTypedWords();
        else if (m_mode == Mode::Settings2) return NVDA_SayBattery();
        else if (m_mode == Mode::WebBrowsing2) return WIN_RefreshSite();
        else return WIN_Delete();
      }
    case GamepadListener::DigitalButton::Y_Pressed: {
        if (m_mode == Mode::WebBrowsing2) return WIN_FocusAddressBar();
        if (m_mode == Mode::DesktopApp2) return WIN_SelectAll();
        if (m_mode == Mode::Settings1) return NVDA_ToggleSpeakCommandKeys();
        if (m_mode == Mode::Settings2) return NVDA_SayTime();
        return WIN_Space();
      }

    case GamepadListener::DigitalButton::Select_Pressed:
        if (m_mode == Mode::DesktopApp2) return WIN_ChangeViewToList();
        if (m_mode == Mode::Settings1) return WIN_OpenWinSettings();
        if (m_mode == Mode::Settings2) return WIN_OpenActionCenter();
        return WIN_StartMenu();
    case GamepadListener::DigitalButton::Start_Pressed: {
        if (m_mode == Mode::DesktopApp2) return WIN_Rename();
        if (m_mode == Mode::TextInput1 || m_mode == Mode::TextInput2) {
            emit eightPointChanged(m_eightPoint = !m_eightPoint);
            logEvent(QString{"EightPoint = "} + (m_eightPoint ? "on" : "off"));
          }
        else if (m_mode == Mode::Settings1) return NVDA_NVDAMenu();
        else if (m_mode == Mode::Settings2) return NVDA_GeneralSettings();
        else return WIN_ContextMenu();
      }


    default: ;
    }
}

void CommandTrigger::setMode(CommandTrigger::Mode mode)
{
  m_mode = mode;

  {
    m_currBrRow = 1;
    m_letter = BrLetter();
    leftDotDone = true;
    rightDotDone = true;
  }

  emit modeChanged(mode);

  switch (mode) {
    case Mode::DesktopApp1:  return logEvent("Mode changed to: DesktopApp1");
    case Mode::WebBrowsing1: return logEvent("Mode changed to: WebBrowsing1");
    case Mode::Settings1:    return logEvent("Mode changed to: Settings1");
    case Mode::DesktopApp2:  return logEvent("Mode changed to: DesktopApp2");
    case Mode::WebBrowsing2: return logEvent("Mode changed to: WebBrowsing2");
    case Mode::Settings2:    return logEvent("Mode changed to: Settings2");
    case Mode::TextInput1:   return logEvent("Mode changed to: Office1");
    case Mode::TextInput2:   return logEvent("Mode changed to: Office2");
    }
}

void CommandTrigger::WIN_Up()
{
  emit logEvent("WIN_Up");
  pressKey(VK_UP, false, false, false, false, true);
}

void CommandTrigger::WIN_Down()
{
  emit logEvent("WIN_Down");
  pressKey(VK_DOWN, false, false, false, false, true);
}

void CommandTrigger::WIN_Left()
{
  emit logEvent("WIN_Left");
  pressKey(VK_LEFT, false, false, false, false, true);
}

void CommandTrigger::WIN_Right()
{
  emit logEvent("WIN_Right");
  pressKey(VK_RIGHT, false, false, false, false, true);
}

void CommandTrigger::WIN_VolumeUp()
{
  emit logEvent("WIN_VolumeUp");
  pressKey(VK_VOLUME_UP);
}

void CommandTrigger::WIN_VolumeDown()
{
  emit logEvent("WIN_VolumeDown");
  pressKey(VK_VOLUME_DOWN);
}

void CommandTrigger::WIN_PrevElement()
{
  emit logEvent("WIN_PrevElement");
  pressKey(VK_TAB, false, false, false, true);
}

void CommandTrigger::WIN_NextElement()
{
  emit logEvent("WIN_NextElement");
  pressKey(VK_TAB);
}

void CommandTrigger::WIN_PrevWindow()
{
  emit logEvent("WIN_PrevWindow");
  pressKey(VK_ESCAPE, false, false, true, true);
}

void CommandTrigger::WIN_NextWindow()
{
  emit logEvent("WIN_NextWindow");
  pressKey(VK_ESCAPE, false, false, true);
}

void CommandTrigger::WIN_NewWindow()
{
  emit logEvent("WIN_NewWindow");
  pressKey(VkKeyScan('n'), false, true);
}

void CommandTrigger::WIN_CloseWindow()
{
  emit logEvent("WIN_CloseWindow");
  pressKey(VK_F4, false, false, true);
}

void CommandTrigger::WIN_PrevTaskBarItem()
{
  emit logEvent("WIN_PrevTaskBarItem");
  pressKey(VkKeyScan('t'), false, false, false, true, false, true);
}

void CommandTrigger::WIN_NextTaskBarItem()
{
  emit logEvent("WIN_NextTaskBarItem");
  pressKey(VkKeyScan('t'), false, false, false, false, false, true);
}

void CommandTrigger::WIN_Activate()
{
  emit logEvent("WIN_Activate");
  pressKey(VK_RETURN);
}

void CommandTrigger::WIN_Cancel()
{
  emit logEvent("WIN_Cancel");
  pressKey(VK_ESCAPE);
}

void CommandTrigger::WIN_Space()
{
  emit logEvent("WIN_Space");
  pressKey(VK_SPACE);
}

void CommandTrigger::WIN_Backspace()
{
  emit logEvent("WIN_Backspace");
  pressKey(VK_BACK);
}

void CommandTrigger::WIN_Delete()
{
  emit logEvent("WIN_Delete");
  pressKey(VK_DELETE);
}

void CommandTrigger::WIN_Rename()
{
  emit logEvent("WIN_Rename");
  pressKey(VK_F2);
}

void CommandTrigger::WIN_NewDirectory()
{
  emit logEvent("WIN_NewDirectory");
  pressKey(VkKeyScan('n'), false, true, false, true);
}

void CommandTrigger::WIN_Home()
{
  emit logEvent("WIN_Home");
  pressKey(VK_HOME);
}

void CommandTrigger::WIN_End()
{
  emit logEvent("WIN_End");
  pressKey(VK_END);
}

void CommandTrigger::WIN_ContextMenu()
{
  emit logEvent("WIN_ContextMenu");
  pressKey(VK_F10, false, false, false, true);
}

void CommandTrigger::WIN_StartMenu()
{
  emit logEvent("WIN_StartMenu");
  pressKey(VK_LWIN);
}

void CommandTrigger::WIN_ApplicationMenu()
{
  emit logEvent("WIN_ApplicationMenu");
  pressKey(VK_MENU);
}

void CommandTrigger::WIN_Search()
{
  emit logEvent("WIN_Search");
  pressKey(VK_F3);
}

void CommandTrigger::WIN_PrevTab()
{
  emit logEvent("WIN_PrevTab");
  pressKey(VK_TAB, false, true, false, true);
}

void CommandTrigger::WIN_NextTab()
{
  emit logEvent("WIN_NextTab");
  pressKey(VK_TAB, false, true);
}

void CommandTrigger::WIN_NewTab()
{
  emit logEvent("WIN_NewTab");
  pressKey(VkKeyScan('t'), false, true);
}

void CommandTrigger::WIN_CloseTab()
{
  emit logEvent("WIN_CloseTab");
  pressKey(VkKeyScan('w'), false, true);
}

void CommandTrigger::WIN_PrevSite()
{
  emit logEvent("WIN_PrevSite");
  pressKey(VK_LEFT, false, false, true);
}

void CommandTrigger::WIN_NextSite()
{
  emit logEvent("WIN_NextSite");
  pressKey(VK_RIGHT, false, false, true);
}

void CommandTrigger::WIN_RefreshSite()
{
  emit logEvent("WIN_RefreshSite");
  pressKey(VK_F5);
}

void CommandTrigger::WIN_StopSite()
{
  emit logEvent("WIN_StopSite");
  pressKey(VK_ESCAPE);
}

void CommandTrigger::WIN_FocusAddressBar()
{
  emit logEvent("WIN_FocusAddressBar");
  pressKey(VkKeyScan('l'), false, true);
}

void CommandTrigger::WIN_Undo()
{
  emit logEvent("WIN_Undo");
  pressKey(VkKeyScan('z'), false, true);
}

void CommandTrigger::WIN_Redo()
{
  emit logEvent("WIN_Redo");
  pressKey(VkKeyScan('y'), false, true);
}

void CommandTrigger::WIN_SelectAll()
{
  emit logEvent("WIN_SelectAll");
  pressKey(VkKeyScan('a'), false, true);
}

void CommandTrigger::WIN_Copy()
{
  emit logEvent("WIN_Copy");
  pressKey(VkKeyScan('c'), false, true);
}

void CommandTrigger::WIN_Cut()
{
  emit logEvent("WIN_Cut");
  pressKey(VkKeyScan('x'), false, true);
}

void CommandTrigger::WIN_Paste()
{
  emit logEvent("WIN_Paste");
  pressKey(VkKeyScan('v'), false, true);
}

void CommandTrigger::WIN_OpenWinSettings()
{
  emit logEvent("WIN_OpenWinSettings");
  pressKey(VkKeyScan('i'), false, false, false, false, false, true);
}

void CommandTrigger::WIN_OpenActionCenter()
{
  emit logEvent("WIN_OpenActionCenter");
  pressKey(VkKeyScan('a'), false, false, false, false, false, true);
}

void CommandTrigger::WIN_SystemProperties()
{
  emit logEvent("WIN_SystemProperties");
  pressKey(VK_PAUSE, false, false, false, false, false, true);
}

void CommandTrigger::WIN_ExpandFolder()
{
  emit logEvent("WIN_ExpandFolder");
  pressKey(VkKeyScan('e'), false, true, false, true);
}

void CommandTrigger::WIN_ChangeViewToList()
{
  emit logEvent("WIN_ChangeViewToList");
  pressKey(0x34, false, true, false, true);
}

void CommandTrigger::WIN_ItemProperties()
{
  emit logEvent("WIN_ItemProperties");
  pressKey(VK_RETURN, false, false, true);
}

void CommandTrigger::WIN_FolderUp()
{
  emit logEvent("WIN_FolderUp");
  pressKey(VK_UP, false, false, true);
}

void CommandTrigger::NVDA_StopReading()
{
  emit logEvent("NVDA_StopReading");
  pressKey(VK_CONTROL);
}

void CommandTrigger::NVDA_PauseResumeReading()
{
  emit logEvent("NVDA_PauseResumeReading");
  pressKey(VK_SHIFT);
}

void CommandTrigger::NVDA_ReadFromCurrentPosition()
{
  emit logEvent("NVDA_ReadFromCurrentPosition");
  pressKey(VK_DOWN, true);
}

void CommandTrigger::NVDA_PrevHeading()
{
  emit logEvent("NVDA_PrevHeading");
  pressKey(VkKeyScan('h'), false, false, false, true);
}

void CommandTrigger::NVDA_NextHeading()
{
  emit logEvent("NVDA_NextHeading");
  pressKey(VkKeyScan('h'));
}

void CommandTrigger::NVDA_ListAllHeadings()
{
  emit logEvent("NVDA_ListAllHeadings");
  pressKey(VK_F7, true);
}

void CommandTrigger::NVDA_PrevForm()
{
  emit logEvent("NVDA_PrevForm");
  pressKey(VkKeyScan('f'), false, false, false, true);
}

void CommandTrigger::NVDA_NextForm()
{
  emit logEvent("NVDA_NextForm");
  pressKey(VkKeyScan('f'));
}

void CommandTrigger::NVDA_PrevParagraph()
{
  emit logEvent("");
  pressKey(VK_RIGHT);
}

void CommandTrigger::NVDA_NextParagraph()
{
  emit logEvent("");
  pressKey(VK_RIGHT);
}

void CommandTrigger::NVDA_PrevTable()
{
  emit logEvent("");
  pressKey(VK_RIGHT);
}

void CommandTrigger::NVDA_NextTable()
{
  emit logEvent("");
  pressKey(VK_RIGHT);
}

void CommandTrigger::NVDA_PrevCellInRow()
{
  emit logEvent("");
  pressKey(VK_RIGHT);
}

void CommandTrigger::NVDA_NextCellInRow()
{
  emit logEvent("");
  pressKey(VK_RIGHT);
}

void CommandTrigger::NVDA_PrevCellInColumn()
{
  emit logEvent("");
  pressKey(VK_RIGHT);
}

void CommandTrigger::NVDA_NextCellInColumn()
{
  emit logEvent("");
  pressKey(VK_RIGHT);
}

void CommandTrigger::NVDA_PrevLink()
{
  emit logEvent("NVDA_PrevLink");
  pressKey(VkKeyScan('k'), false, false, false, true);
}

void CommandTrigger::NVDA_NextLink()
{
  emit logEvent("NVDA_NextLink");
  pressKey(VkKeyScan('k'));
}

void CommandTrigger::NVDA_SayClipboard()
{
  emit logEvent("NVDA_SayClipboard");
  pressKey(VkKeyScan('c'), true);;
}

void CommandTrigger::NVDA_SayCurrentObject()
{
  emit logEvent("NVDA_SayCurrentObject");
  pressKey(VK_TAB, true);
}

void CommandTrigger::NVDA_SayTitleBar()
{
  emit logEvent("NVDA_SayTitleBar");
  pressKey(VkKeyScan('t'), true);
}

void CommandTrigger::NVDA_SayWindow()
{
  emit logEvent("NVDA_SayWindow");
  pressKey(VkKeyScan('b'), true);
}

void CommandTrigger::NVDA_SayStatusBar()
{
  emit logEvent("NVDA_SayStatusBar");
  pressKey(VK_END, true);
}

void CommandTrigger::NVDA_ToggleBrowseFocusMode()
{
  emit logEvent("NVDA_ToggleBrowseFocusMode");
  pressKey(VK_SPACE, true);
}

void CommandTrigger::NVDA_ElementsListDialog()
{
  emit logEvent("NVDA_ElementsListDialog");
  pressKey(VK_RIGHT);
}

void CommandTrigger::WIN_FindDialog()
{
  emit logEvent("WIN_FindDialog");
  pressKey(VkKeyScan('f'), false, true);
}

void CommandTrigger::NVDA_NVDAMenu()
{
  emit logEvent("NVDA_NVDAMenu");
  pressKey(VkKeyScan('n'), true);
}

void CommandTrigger::NVDA_GeneralSettings()
{
  emit logEvent("NVDA_GeneralSettings");
  pressKey(VkKeyScan('g'), true, true);
}

void CommandTrigger::NVDA_SpeakSlower()
{
  emit logEvent("NVDA_SpeakSlower");
  pressKey(VK_DOWN, true, true);
}

void CommandTrigger::NVDA_SpeakFaster()
{
  emit logEvent("NVDA_SpeakFaster");
  pressKey(VK_UP, true, true);
}

void CommandTrigger::NVDA_ChangeLanguage()
{
  emit logEvent("NVDA_ChangeLanguage");
  pressKey(VK_RIGHT); //TODO
}

void CommandTrigger::NVDA_ToggleSpeakTypedWords()
{
  emit logEvent("NVDA_ToggleSpeakTypedWords");
  pressKey(0x33, true);
}

void CommandTrigger::NVDA_ToggleSpeakCommandKeys()
{
  emit logEvent("NVDA_ToggleSpeakCommandKeys");
    pressKey(0x34, true);
}

void CommandTrigger::NVDA_ToggleSpeakPunktuation()
{
  emit logEvent("NVDA_ToggleSpeakPunktuation");
  pressKey(VkKeyScan('p'), true);
}

void CommandTrigger::NVDA_SayTime()
{
  emit logEvent("");
  pressKey(VK_RIGHT);
}

void CommandTrigger::NVDA_SayBattery()
{
  emit logEvent("");
  pressKey(VK_RIGHT);
}

void CommandTrigger::WIN_InitDictation()
{
  emit logEvent("WIN_InitDictation");
  // TODO
}

void CommandTrigger::pressKey(int keyCode, bool insert, bool ctrl, bool alt, bool shift, bool extended, bool super)
{
  INPUT input;

  input.type = INPUT_KEYBOARD;
  input.ki.time = 0;
  input.ki.dwExtraInfo = 0;
  input.ki.wScan = 0;
  input.ki.dwFlags = extended ? KEYEVENTF_EXTENDEDKEY : 0;

  // Press...

  if (insert) {
      input.ki.wVk = VK_INSERT;
      SendInput( 1, &input, sizeof( INPUT ) );
    }

  if (ctrl) {
      input.ki.wVk = VK_CONTROL;
      SendInput( 1, &input, sizeof( INPUT ) );
    }

  if (alt) {
      input.ki.wVk = VK_RMENU;
      SendInput( 1, &input, sizeof( INPUT ) );
    }

  if (shift) {
      input.ki.wVk = VK_SHIFT;
      SendInput( 1, &input, sizeof( INPUT ) );
    }

  if (super) {
      input.ki.wVk = VK_LWIN;
      SendInput( 1, &input, sizeof( INPUT ) );
  }

  input.ki.wVk = keyCode;
  SendInput( 1, &input, sizeof( INPUT ) );

  // Release...

  input.ki.dwFlags = KEYEVENTF_KEYUP | (extended ? KEYEVENTF_EXTENDEDKEY : 0);
  SendInput( 1, &input, sizeof( INPUT ) );

  if (insert) {
      input.ki.wVk = VK_INSERT;
      SendInput( 1, &input, sizeof( INPUT ) );
    }

  if (ctrl) {
      input.ki.wVk = VK_CONTROL;
      SendInput( 1, &input, sizeof( INPUT ) );
    }

  if (alt) {
      input.ki.wVk = VK_RMENU;
      SendInput( 1, &input, sizeof( INPUT ) );
    }

  if (shift) {
      input.ki.wVk = VK_SHIFT;
      SendInput( 1, &input, sizeof( INPUT ) );
    }

  if (super) {
      input.ki.wVk = VK_LWIN;
      SendInput( 1, &input, sizeof( INPUT ) );
  }
}

void CommandTrigger::toggle(bool &b)
{
  b = !b;
}

void CommandTrigger::brLetterEntry()
{
  auto letter { (wchar_t)m_letter };

  logEvent("Entering letter: " + QString(letter));
  m_letter = BrLetter();
  m_currBrRow = 1;
  leftDotDone = true;
  rightDotDone = true;

  bool alt = false;

  if (QChar{letter} == L'ą') {
      alt = true;
      letter = L'a';
  } else if (QChar{letter} == L'Ą') {
      alt = true;
      letter = L'A';
  }  else if (QChar{letter} == L'ć') {
      alt = true;
      letter = L'c';
  } else if (QChar{letter} == L'Ć') {
      alt = true;
      letter = L'C';
  } else if (QChar{letter} == L'ę') {
      alt = true;
      letter = L'e';
  } else if (QChar{letter} == L'Ę') {
      alt = true;
      letter = L'E';
  } else if (QChar{letter} == L'ł') {
      alt = true;
      letter = L'l';
  } else if (QChar{letter} == L'Ł') {
      alt = true;
      letter = L'L';
  } else if (QChar{letter} == L'ń') {
      alt = true;
      letter = L'n';
  } else if (QChar{letter} == L'Ń') {
      alt = true;
      letter = L'N';
  } else if (QChar{letter} == L'ó') {
      alt = true;
      letter = L'o';
  } else if (QChar{letter} == L'Ó') {
      alt = true;
      letter = L'O';
  } else if (QChar{letter} == L'ś') {
      alt = true;
      letter = L's';
  } else if (QChar{letter} == L'Ś') {
      alt = true;
      letter = L'S';
  } else if (QChar{letter} == L'ź') {
      alt = true;
      letter = L'x';
  } else if (QChar{letter} == L'Ź') {
      alt = true;
      letter = L'X';
  } else if (QChar{letter} == L'ż') {
      alt = true;
      letter = L'z';
  } else if (QChar{letter} == L'Ż') {
      alt = true;
      letter = L'Z';
  }

  pressKey(VkKeyScan(letter), false, false, alt, QChar{letter}.isUpper());
}

//https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes
