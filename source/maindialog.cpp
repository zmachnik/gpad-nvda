#include "maindialog.h"
#include "ui_maindialog.h"

#include <QTextStream>
#include <QApplication>
#include <QScrollBar>
#include <QCommandLinkButton>

MainDialog::MainDialog(QWidget *parent)
  : QDialog(parent)
  , ui(new Ui::MainDialog)
  , m_notifier(m_TTS)
{
  loadStylesheet();

  ui->setupUi(this);

  m_trayIcon.setIcon( QIcon{":/graphics/icons/mainIcon.png"} );
  m_trayIcon.show();

  connect(&m_trayIcon, &QSystemTrayIcon::activated, this, [this](){
      this->show(); this->setWindowState(Qt::WindowState::WindowActive); });

  setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
  setWindowFlags(windowFlags() | Qt::WindowMinimizeButtonHint);

  connect(ui->logTE, &QPlainTextEdit::textChanged, this, [this]() {
      this->ui->logTE->verticalScrollBar()->setValue(this->ui->logTE->verticalScrollBar()->maximum());
    } );

  connect(&m_gamepadListener, &GamepadListener::noGamepadFound, this, [this](){
      addLogMessage("No gamepad detected!");
    });

  connect(&m_gamepadListener, &GamepadListener::gamepadConnected, this, [this](){
      addLogMessage("Gamepad connected!");
    });

  connect(&m_gamepadListener, &GamepadListener::gamepadDisconnected, this, [this](){
      addLogMessage("Gamepad disconnected!");
    });


  connect(&m_gamepadListener, &GamepadListener::buttonAction,
          &m_commandTrigger, &CommandTrigger::translateButton);

  connect(&m_commandTrigger, &CommandTrigger::logEvent, this, &MainDialog::addLogMessage);

  m_notifier.startUp();


  connect(&m_commandTrigger, &CommandTrigger::modeChanged, this, [this](CommandTrigger::Mode mode) {
    if (mode == CommandTrigger::Mode::DesktopApp1) m_notifier.desktopApp(false);
    else if (mode == CommandTrigger::Mode::DesktopApp2) m_notifier.desktopApp(true);
    else if (mode == CommandTrigger::Mode::WebBrowsing1) m_notifier.webBrowsing(false);
    else if (mode == CommandTrigger::Mode::WebBrowsing2) m_notifier.webBrowsing(true);
    else if (mode == CommandTrigger::Mode::TextInput1) m_notifier.textInput(false);
    else if (mode == CommandTrigger::Mode::TextInput2) m_notifier.textInput(true);
    else if (mode == CommandTrigger::Mode::Settings1) m_notifier.settings(false);
    else if (mode == CommandTrigger::Mode::Settings2) m_notifier.settings(true);
  });

  connect(&m_commandTrigger, &CommandTrigger::eightPointChanged, this, [this](bool on) {
    m_notifier.eightPoint(on);
  });

  connect(ui->runNvdaCLB, &QCommandLinkButton::clicked,
          &m_mgr, &NVDAProcessManager::runNVDA);

  applySettings();

  if (m_mgr.isNVDARunning()) {
      ui->runNvdaCLB->setText(QString{"NVDA jest uruchomiony"});
      ui->runNvdaCLB->setEnabled(false);
  }

  ui->activitiesLW->setCurrentItem(0);
}

MainDialog::~MainDialog()
{
  delete ui;
}

void MainDialog::keyPressEvent(QKeyEvent *e)
{
  if (e->key() != Qt::Key_Escape)
    QDialog::keyPressEvent(e);
}

void MainDialog::changeEvent(QEvent *event)
{
  if (event->type() == QEvent::WindowStateChange)
    if (isMinimized()) hide();
}

void MainDialog::loadStylesheet()
{
  QFile stylesheetFile { ":misc/misc/stylesheet.qss" };
  if (stylesheetFile.open(QFile::ReadOnly | QFile::Text)) {

      QString styleSheet;

      int globalFontSize { 20 };

      styleSheet += "QWidget { font-size: ";
      styleSheet += QString::number(globalFontSize);
      styleSheet += "px; }\n";

      QTextStream ts { &stylesheetFile };
      styleSheet += ts.readAll();
      qApp->setStyleSheet(styleSheet);
    }
}

void MainDialog::applySettings()
{
  if (!m_settings.contains("path_nvda"))
    m_settings.setValue("path_nvda", "C:\\Program Files (x86)\\NVDA\\nvda.exe");

  m_mgr.setNVDA_Path(m_settings.value("path_nvda").toString());
}

void MainDialog::addLogMessage(const QString & message)
{
  ui->logTE->appendPlainText(message);
}
