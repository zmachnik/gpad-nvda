#ifndef NVDAPROCESSMANAGER_H
#define NVDAPROCESSMANAGER_H

#include <QObject>

class NVDAProcessManager : public QObject
{
  Q_OBJECT

public:
  explicit NVDAProcessManager(QObject *parent = nullptr);

  bool isNVDARunning();
  void setNVDA_Path(const QString & path);

public slots:

  bool runNVDA();

private:

  QString m_NVDAPath;

signals:
  void NVDA_Started();

};

#endif // NVDAPROCESSMANAGER_H
