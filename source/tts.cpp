#include "tts.h"

TTS::TTS()
{ 
  // TODO: Allow user to select any TTS voice

  const auto & engines = QTextToSpeech::availableEngines();
  for (const auto & engine : engines)
    if (engine.contains(QString{"sapi"}, Qt::CaseInsensitive))
      m_speech = new QTextToSpeech {engine, this};

  if (m_speech) {

    for (const auto & locale : m_speech->availableLocales()) {
      if (locale.language() == QLocale::Polish) {
          m_speech->setLocale(locale);
          break;
      }
    }

    for (const auto & voice : m_speech->availableVoices()) {
        if (voice.name().contains(QString{"Paulina"}, Qt::CaseInsensitive)) {
          m_speech->setVoice(voice);
          break;
      }
    }

  }
}

void TTS::say(const QString &text) const
{
  if (m_speech) m_speech->say(text);
}
