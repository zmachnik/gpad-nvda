#include "audionotify.h"

#include <QSound>

AudioNotify::AudioNotify(const TTS &tts)
  : m_tts{tts}
{
  m_startUp.setSource(QUrl{"qrc:///sound/sounds/startup.wav"});

  connect(&m_startUp, &QSoundEffect::playingChanged, this, [this](){
      if (!m_startUp.isPlaying()) m_tts.say(QString{"Dzień dobry"});
  });
}

void AudioNotify::startUp()
{
  m_startUp.play();
}

void AudioNotify::desktopApp(bool pressed)
{
  pressed ? QSound::play(":/sound/sounds/modeB1.wav") : QSound::play(":/sound/sounds/modeB2.wav");
}

void AudioNotify::webBrowsing(bool pressed)
{
  pressed ? QSound::play(":/sound/sounds/modeD1.wav") : QSound::play(":/sound/sounds/modeD2.wav");
}

void AudioNotify::textInput(bool pressed)
{
  pressed ? QSound::play(":/sound/sounds/modeC1.wav") : QSound::play(":/sound/sounds/modeC2.wav");
}

void AudioNotify::settings(bool pressed)
{
  pressed ? QSound::play(":/sound/sounds/modeA1.wav") : QSound::play(":/sound/sounds/modeA2.wav");
}

void AudioNotify::eightPoint(bool on)
{
  on ? QSound::play(":/sound/sounds/8point.wav") : QSound::play(":/sound/sounds/6point.wav");
}

