#ifndef TTS_H
#define TTS_H

#include <QTextToSpeech>

class TTS : public QObject
{
  Q_OBJECT

public:
  TTS();

public slots:
  void say(const QString & text) const;

protected:
private:

  QTextToSpeech * m_speech = nullptr;
  QVector<QVoice> m_voices;
};

#endif // TTS_H
