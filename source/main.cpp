#include "maindialog.h"

#include <QApplication>

int main(int argc, char *argv[])
{
  QCoreApplication::setOrganizationName("ZM");
  QCoreApplication::setApplicationName("Gamepad2NVDA");

  QApplication a(argc, argv);

  MainDialog w;
  w.show();
  return a.exec();
}
