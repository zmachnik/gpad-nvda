QT  += core gui gamepad multimedia texttospeech

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    audionotify.cpp \
    brletter.cpp \
    commandtrigger.cpp \
    gamepadlistener.cpp \
    main.cpp \
    maindialog.cpp \
    nvdaprocessmanager.cpp \
    tts.cpp

HEADERS += \
    audionotify.h \
    brletter.h \
    commandtrigger.h \
    gamepadlistener.h \
    maindialog.h \
    nvdaprocessmanager.h \
    tts.h

FORMS += \
    maindialog.ui

RESOURCES += \
    Resources.qrc

