#include "gamepadlistener.h"

#include <memory>

#include <QtMath>

GamepadListener::GamepadListener()
{
  connect(QGamepadManager::instance(),
          &QGamepadManager::gamepadConnected, this, [this](){
      initDefaultGamepad();
      emit gamepadConnected();
    });

  connect(QGamepadManager::instance(),
          &QGamepadManager::gamepadDisconnected, this, [this](){
      emit gamepadDisconnected();
    });
}

void GamepadListener::initDefaultGamepad()
{
  m_gamepad.reset();

  auto gamepads = QGamepadManager::instance()->connectedGamepads();

  if (gamepads.isEmpty()) {
      emit noGamepadFound();
      return;
    }

  m_gamepad = std::make_unique<QGamepad>(*gamepads.begin(), this);

  m_currLStickDirection = stickToButton(m_gamepad->axisLeftX(), m_gamepad->axisLeftY());
  m_currRStickDirection = stickToButton(m_gamepad->axisRightX(), m_gamepad->axisRightY());

  m_currLTriggerPosition = triggerToButton(m_gamepad->buttonL2());
  m_currRTriggerPosition = triggerToButton(m_gamepad->buttonR2());

  // ABXY

  connect(m_gamepad.get(), &QGamepad::buttonAChanged, this, [this](bool pressed){
      if (pressed) emit buttonAction(DigitalButton::A_Pressed);
      else emit buttonAction(DigitalButton::A_Released);
    });

  connect(m_gamepad.get(), &QGamepad::buttonBChanged, this, [this](bool pressed){
      if (pressed) emit buttonAction(DigitalButton::B_Pressed);
      else emit buttonAction(DigitalButton::B_Released);
    });

  connect(m_gamepad.get(), &QGamepad::buttonXChanged, this, [this](bool pressed){
      if (pressed) emit buttonAction(DigitalButton::X_Pressed);
      else emit buttonAction(DigitalButton::X_Released);
    });

  connect(m_gamepad.get(), &QGamepad::buttonYChanged, this, [this](bool pressed){
      if (pressed) emit buttonAction(DigitalButton::Y_Pressed);
      else emit buttonAction(DigitalButton::Y_Released);
    });

  // Menu buttons

  connect(m_gamepad.get(), &QGamepad::buttonGuideChanged, this, [this](bool pressed){
      if (pressed) emit buttonAction(DigitalButton::Guide_Pressed);
    });

  connect(m_gamepad.get(), &QGamepad::buttonSelectChanged, this, [this](bool pressed){
      if (pressed) emit buttonAction(DigitalButton::Select_Pressed);
    });

  connect(m_gamepad.get(), &QGamepad::buttonStartChanged, this, [this](bool pressed){
      if (pressed) emit buttonAction(DigitalButton::Start_Pressed);
    });

  // Side buttons

  connect(m_gamepad.get(), &QGamepad::buttonL1Changed, this, [this](bool pressed){
      if (pressed) emit buttonAction(DigitalButton::L1_Pressed);
      else emit buttonAction(DigitalButton::L1_Released);
    });

  connect(m_gamepad.get(), &QGamepad::buttonL3Changed, this, [this](bool pressed){
      if (pressed) emit buttonAction(DigitalButton::L3_Pressed);
    });

  connect(m_gamepad.get(), &QGamepad::buttonR1Changed, this, [this](bool pressed){
      if (pressed) emit buttonAction(DigitalButton::R1_Pressed);
      else emit buttonAction((DigitalButton::R1_Released));
    });

  connect(m_gamepad.get(), &QGamepad::buttonR3Changed, this, [this](bool pressed){
      if (pressed) emit buttonAction(DigitalButton::R3_Pressed);
    });

  // D-Pad

  connect(m_gamepad.get(), &QGamepad::buttonUpChanged, this, [this](bool pressed){
      if (pressed) emit buttonAction(DigitalButton::Up_Pressed);
      else emit buttonAction(DigitalButton::Up_Released);
    });

  connect(m_gamepad.get(), &QGamepad::buttonDownChanged, this, [this](bool pressed){
      if (pressed) emit buttonAction(DigitalButton::Down_Pressed);
      else emit buttonAction(DigitalButton::Down_Released);
    });

  connect(m_gamepad.get(), &QGamepad::buttonLeftChanged, this, [this](bool pressed){
      if (pressed) emit buttonAction(DigitalButton::Left_Pressed);
      else emit buttonAction(DigitalButton::Left_Released);
    });

  connect(m_gamepad.get(), &QGamepad::buttonRightChanged, this, [this](bool pressed){
      if (pressed) emit buttonAction(DigitalButton::Right_Pressed);
      else emit buttonAction(DigitalButton::Right_Released);
    });

  // Analog Sticks

  connect(m_gamepad.get(), &QGamepad::axisLeftXChanged, this, &GamepadListener::digitizeLStick);
  connect(m_gamepad.get(), &QGamepad::axisLeftYChanged, this, &GamepadListener::digitizeLStick);
  connect(m_gamepad.get(), &QGamepad::axisRightXChanged, this, &GamepadListener::digitizeRStick);
  connect(m_gamepad.get(), &QGamepad::axisRightYChanged, this, &GamepadListener::digitizeRStick);

  // Analog Triggers

  connect(m_gamepad.get(), &QGamepad::buttonL2Changed, this, &GamepadListener::digitizeLTrigger);
  connect(m_gamepad.get(), &QGamepad::buttonL2Changed, this, &GamepadListener::digitizeLTrigger);
  connect(m_gamepad.get(), &QGamepad::buttonR2Changed, this, &GamepadListener::digitizeRTrigger);
  connect(m_gamepad.get(), &QGamepad::buttonR2Changed, this, &GamepadListener::digitizeRTrigger);
}

GamepadListener::StickPosition GamepadListener::stickToButton(double x, double y)
{
  const static double centerZone = 0.6;

  if (x < -centerZone || x > centerZone || y < -centerZone || y > centerZone)
    {
      if (x > 0 && x >  qFabs(y)) return StickPosition::Right;
      if (x < 0 && x < -qFabs(y)) return StickPosition::Left;
      if (y > 0 && y >  qFabs(x)) return StickPosition::Down;
      if (y < 0 && y < -qFabs(x)) return StickPosition::Up;
    }

  return StickPosition::Center;
}

bool GamepadListener::triggerToButton(double x)
{
  const static double offZone = 0.6;

  return x > offZone;
}

void GamepadListener::digitizeLStick()
{
  auto direction = stickToButton(m_gamepad->axisLeftX(), m_gamepad->axisLeftY());
  if (direction != m_currLStickDirection) {
      m_currLStickDirection = direction;
      switch (direction) {
        case StickPosition::Up: emit buttonAction(DigitalButton::LS_Up_Pressed); break;
        case StickPosition::Down: emit buttonAction(DigitalButton::LS_Down_Pressed); break;
        case StickPosition::Left: emit buttonAction(DigitalButton::LS_Left_Pressed); break;
        case StickPosition::Right: emit buttonAction(DigitalButton::LS_Right_Pressed); break;
        default: ;
        }
    }
}

void GamepadListener::digitizeRStick()
{
  auto direction = stickToButton(m_gamepad->axisRightX(), m_gamepad->axisRightY());
  if (direction != m_currRStickDirection) {
      m_currRStickDirection = direction;
      switch (direction) {
        case StickPosition::Up: emit buttonAction(DigitalButton::RS_Up_Pressed); break;
        case StickPosition::Down: emit buttonAction(DigitalButton::RS_Down_Pressed); break;
        case StickPosition::Left: emit buttonAction(DigitalButton::RS_Left_Pressed); break;
        case StickPosition::Right: emit buttonAction(DigitalButton::RS_Right_Pressed); break;
        default: ;
        }
    }
}

void GamepadListener::digitizeLTrigger()
{
  auto position = triggerToButton(m_gamepad->buttonL2());

  if (position != m_currLTriggerPosition) {
      m_currLTriggerPosition = position;
      if (position) emit buttonAction(DigitalButton::L2_Pressed);
      else emit buttonAction((DigitalButton::L2_Released));
    }
}

void GamepadListener::digitizeRTrigger()
{
  auto position = triggerToButton(m_gamepad->buttonR2());
  if (position != m_currRTriggerPosition) {
      m_currRTriggerPosition = position;
      if (position) emit buttonAction(DigitalButton::R2_Pressed);
      else emit buttonAction((DigitalButton::R2_Released));
    }
}
