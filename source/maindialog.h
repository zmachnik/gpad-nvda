#ifndef MAINDIALOG_H
#define MAINDIALOG_H

#include <QDialog>
#include <QSystemTrayIcon>
#include <QSettings>

#include "gamepadlistener.h"
#include "commandtrigger.h"
#include "audionotify.h"
#include "nvdaprocessmanager.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainDialog; }
QT_END_NAMESPACE

class MainDialog : public QDialog
{
  Q_OBJECT

public:
  MainDialog(QWidget *parent = nullptr);
  ~MainDialog();

protected:
  void keyPressEvent(QKeyEvent * e) override;
  void changeEvent(QEvent * event) override;

private:
  Ui::MainDialog *ui;

  QSystemTrayIcon m_trayIcon;

  GamepadListener m_gamepadListener;
  CommandTrigger m_commandTrigger;

  TTS m_TTS;
  AudioNotify m_notifier;

  NVDAProcessManager m_mgr;

  QSettings m_settings;

  static void loadStylesheet();

  void applySettings();

private slots:
  void addLogMessage(const QString &message);
};
#endif // MAINDIALOG_H
